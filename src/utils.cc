#include "sphero/utils.h"

#include <assert.h> /* assert */

using namespace std;
using namespace sphero;

uint8_t utils::checksum(const std::vector<uint8_t> &data, const bool containsCHK) {

    assert(data.size() > 2 + containsCHK);

    uint8_t x = 0;

    for(int i = 2 ; i < data.size() - containsCHK; x += data.at(i++));

    return (x ^= 0xFF);
}

uint8_t utils::scharToUint8(const std::vector<uint8_t> &c, const int start) {

    assert(start <= c.size());

    uint8_t dec = c.at(start);

    return dec;

}

uint16_t utils::scharToUint16(const std::vector<uint8_t> &c, const int start) {

    assert(start-1 <= c.size());

    uint16_t dec  =  c.at(start) << 8
                  |  c.at(start+1);

    return dec;

}

int16_t utils::scharToInt16(const std::vector<uint8_t> &c, const int start) {

    assert(start-1 <= c.size());

    int16_t dec = c.at(start) << 8
                | c.at(start+1);

    return dec;
}

// https://stackoverflow.com/a/109025/3294674
int utils::numberOfSetBits(uint32_t i) {

     i = i - ((i >> 1) & 0x55555555);
     i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
     return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;

}

std::pair<uint8_t, uint8_t> utils::splitUint16(const uint16_t &value) {

    // std::bitset<16> value_bit(value);
    // std::bitset<8> first((uint8_t)(value >> 8));
    // std::bitset<8> second((uint8_t)value);

    uint8_t first  = (uint8_t)(value >> 8);
    uint8_t second = (uint8_t)(value);

    return make_pair(first, second);

}

