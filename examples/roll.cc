#include <sphero/sphero.h>

#include <iostream>
#include <unistd.h> /* sleep */

//#include <chrono> /*time in milliseconds */
//using namespace std::chrono;

int main(int argc, char *argv[])
{

    if ( argc != 2 ) {
        std::cout << "Please provide an address to a Sphero." << std::endl;
        return 1;
    }

    sphero::Sphero sp;

    // empty callback when data received
    std::function<void(std::vector<int16_t> &data)> onReceive = {};

    // mac address
    std::string mac = argv[1];

    // callback when connection established
    std::function<void()> onConnect = []() {
        std::cout << "Sphero connected" << std::endl;
    };

    // connect to a Sphero
    sp.connectBlocking(mac, onReceive, onConnect);

    // send roll
    sp.roll(100, 100);


    // keep alive
    for(;;) {
        sleep(5);
        sp.keepalive();

    }

}
