#ifndef SPHERO_H
#define SPHERO_H

// data structures
#include <stdint.h> /* uint8_t */
#include <vector>
#include <tuple>

// output
#include <iostream> /* string, stream */
#include <assert.h> /* assert */


#include <blepp/blestatemachine.h>
#include <sphero/utils.h>

namespace sphero {

// flags
const uint8_t sop1      = 0xFF;
const uint8_t sop2ackno = 0xFF; // 0xFE acknowledgement
const uint8_t sop2async = 0xFE; // 0xFE async message

// UUIDs
const BLEPP::UUID sBLE  = BLEPP::UUID("22bb746f-2bb0-7554-2d6f-726568705327");
const BLEPP::UUID cTXP  = BLEPP::UUID("22bb746f-2bb2-7554-2d6f-726568705327");
const BLEPP::UUID cADoS = BLEPP::UUID("22bb746f-2bbd-7554-2d6f-726568705327");
const BLEPP::UUID cWake = BLEPP::UUID("22bb746f-2bbf-7554-2d6f-726568705327");

const BLEPP::UUID sCtrl = BLEPP::UUID("22bb746f-2ba0-7554-2d6f-726568705327");
const BLEPP::UUID cCmd  = BLEPP::UUID("22bb746f-2ba1-7554-2d6f-726568705327");
const BLEPP::UUID cRsp  = BLEPP::UUID("22bb746f-2ba6-7554-2d6f-726568705327");


typedef std::tuple<int, int, std::string, std::string> sensorT;

/*! main interface for interacting with Sphero */

class Sphero
{

public:
    Sphero();
    ~Sphero();

    void putInDevMode();

    void connectBlocking(const std::string mac_address, std::function<void(std::vector<int16_t> &)> onReceive, std::function<void()> onConnect = []() {});
    void read();

    // API
    void ping(); //!< pings the robot and awaits a repsonse
    void keepalive(); //!< keep connection alive with pinging the robot without requesting an ack package
    void ping(const bool reqACK); //!< pings the robot and awaits a repsonse (optionally)
    void setRGBLEDoutput(const uint8_t r, const uint8_t g, const uint8_t b, const bool persistent = false);
    void roll(const uint8_t speed, const uint16_t heading, const bool reqACK = false);
    void setStabilization(const bool flag);
    void setRotationRate(const uint degreesPerSecond = 200);
    void setDataStreaming(const std::vector<uint8_t> &mask, const std::vector<uint8_t> &mask2meta, const uint freq = 10);
    void setDataStreaming(const uint32_t mask1 = 0, const uint32_t mask2 = 0, const uint freq = 10);
    void setRawMotorValues(const uint8_t lMode, const uint8_t lPower, const uint8_t rMode, const uint8_t rPower, const bool reqACK = false);

    void setInactivityTimeout(const uint16_t seconds);

    sensorT getSensorTuple(const uint i) const {

        assert(i < _streamDataMeta.size());
        assert(!_streamDataMeta.empty());

        return _streamDataMeta.at(i);

    }



    int getSensorMin(const uint i) const {

        assert(i < _streamDataMeta.size());
        assert(!_streamDataMeta.empty());

        return std::get<0>(_streamDataMeta.at(i));

    }

    int getSensorMax(const uint i) const {

        assert(i < _streamDataMeta.size());
        assert(!_streamDataMeta.empty());

        return std::get<1>(_streamDataMeta.at(i));

    }

    std::string getSensorShortName(const uint i) const {

        assert(i < _streamDataMeta.size());
        assert(!_streamDataMeta.empty());

        return std::get<2>(_streamDataMeta.at(i));

    }

    std::string getSensorDescription(const uint i) const {

        assert(i < _streamDataMeta.size());
        assert(!_streamDataMeta.empty());

        return std::get<3>(_streamDataMeta.at(i));
    }

    uint getStreamedSensorNum() const {
        return utils::numberOfSetBits(_streamingMask) + utils::numberOfSetBits(_streamingMask2);
    }

    // get bitmask
    virtual std::pair<uint32_t, uint32_t> getBitmasks(const std::vector<std::string>& sensors) const {

        uint32_t mask1 = 0;
        uint32_t mask2 = 0;

        for (int i=0;i<sensors.size();i++)
            for (int x=0; x<31; x++)
                mask1 |= getSensorBit(sensors.at(i));


        for (int i=0;i<sensors.size();i++)
            for (int x=0; x<31; x++)
                mask2 |= getSensorBit(sensors.at(i), true);

        return std::make_pair(mask1, mask2);
    }

private:
    BLEPP::BLEGATTStateMachine _gatt;
    bool _deviceFound;
    std::vector<uint8_t> _streamingMaskVector;
    uint32_t _streamingMask;
    uint32_t _streamingMask2;
    std::vector<sensorT> _streamDataMeta;

    std::function<void(std::vector<int16_t> &data)> _onReceive;

    void parse(const uint8_t* d, const int length);
    void parseASYNC(const std::vector<uint8_t> &value);

    // helper
    std::vector<int16_t> getAsyncPayload(const std::vector<uint8_t> &ba);

sensorT getSensorMeta(const uint i, const bool mask2 = false) const {

        assert(i < 32);

        if (!mask2) {
            return mask1meta.at(i);
        } else {
            return mask2meta.at(i);
        }
    }

    void sendCommand(const uint8_t did, const uint8_t cid, std::vector<uint8_t> &data, bool requestACK = 1);
    void sendCommandBlocking(const uint8_t did, const uint8_t cid, std::vector<uint8_t> &data, bool requestACK = 1);


    std::vector<uint8_t> _buffer;
    int responseType(const std::vector<uint8_t> &pkg) const;
    int isValidResponse(const std::vector<uint8_t> &pkg) const;
    int getExpectedSize(const std::vector<uint8_t> &pkg, const int type) const;
    std::vector<u_int8_t> extractPackageFromBuffer(std::vector<uint8_t> &pkg, const int type);
    bool isInvalidResponse(const std::vector<uint8_t> &pkg) const;

    virtual uint32_t getSensorBit(const std::string s, const bool mask2 = false) const {

        if (!mask2) {

            for (uint i = 0; i < mask1meta.size(); i++)
                if (s == std::get<2>(mask1meta.at(i)))
                    return (1 << (31-i));
        }

        if (mask2) {

            for (uint i = 0; i < mask2meta.size(); i++)
                if (s == std::get<2>(mask2meta.at(i)))
                    return (1 << (31-i));
        }

        return 0;
    }

    // definiton
public:
    // TODO: max, min not proper for real experiments
    // optional config file needed (for now, hard coded in controller)
    // source https://sdk.sphero.com/api-reference/api-quick-reference/#sphero-set-data-streaming-11h
    const std::vector<sensorT> mask1meta = {
        {-2048, 2047, "accelX", "accelerometer axis X, raw -2048 to 2047 4mG"},
        {-2048, 2047, "accelY", "accelerometer axis Y, raw -2048 to 2047 4mG"},
        {-2048, 2047, "accelZ", "accelerometer axis Z, raw -2048 to 2047 4mG"},
        {-32768, 32767, "gyroX", "gyro axis X, raw -32768 to 32767 0.068 degrees"},
        {-32768, 32767, "gyroY", "gyro axis Y, raw -32768 to 32767 0.068 degrees"},
        {-32768, 32767, "gyroZ", "gyro axis Z, raw -32768 to 32767 0.068 degrees"},
        {0, 0, "Reserved", "Reserved"},
        {0, 0, "Reserved", "Reserved"},
        {0, 0, "Reserved", "Reserved"},
        // or{-32768, 32767, "motorEMFright", "right motor back EMF, raw -32768 to 32767 22.5 cm"},
        // or{-32768, 32767, "motorEMFleft", "left motor back EMF, raw -32768 to 32767 22.5 cm"},
        {-1000, 1000, "motorEMFright", "right motor back EMF, raw -32768 to 32767 22.5 cm"},
        {-1000, 1000, "motorEMFleft", "left motor back EMF, raw -32768 to 32767 22.5 cm"},
        {-2048, 2047, "motorPWMleft", "left motor, PWM, raw -2048 to 2047 duty cycle"},
        {-2048, 2047, "motorPWMright", "right motor, PWM raw -2048 to 2047 duty cycle"},
        // or{-179, 180, "IMUpitch", "IMU pitch angle, filtered -179 to 180 degrees"},
        // or{-179, 180, "IMUroll", "IMU roll angle, filtered -179 to 180 degrees"},
        // or{-179, 180, "IMUyaw", "IMU yaw angle, filtered -179 to 180 degrees"},
        {-180, 179, "IMUpitch", "IMU pitch angle, filtered -179 to 180 degrees"},
        {-90, 89, "IMUroll", "IMU roll angle, filtered -179 to 180 degrees"},
        {-180, 179, "IMUyaw", "IMU yaw angle, filtered -179 to 180 degrees"},
        // or{-32768, 32767, "accelX_f", "accelerometer axis X, filtered -32768 to 32767 1/4096 G"},
        // or{-32768, 32767, "accelY_f", "accelerometer axis Y, filtered -32768 to 32767 1/4096 G"},
        // or{-32768, 32767, "accelZ_f", "accelerometer axis Z, filtered -32768 to 32767 1/4096 G"},
        {-15000, 15000, "accelX_f", "accelerometer axis X, filtered -32768 to 32767 1/4096 G"},
        {-15000, 15000, "accelY_f", "accelerometer axis Y, filtered -32768 to 32767 1/4096 G"},
        {-15000, 15000, "accelZ_f", "accelerometer axis Z, filtered -32768 to 32767 1/4096 G"},
        // or{-20000, 20000, "gyroX_f", "gyro axis X, filtered -20000 to 20000 0.1 dps"},
        // or{-20000, 20000, "gyroY_f", "gyro axis Y, filtered -20000 to 20000 0.1 dps"},
        // or{-20000, 20000, "gyroZ_f", "gyro axis Z, filtered -20000 to 20000 0.1 dps"},
        {-9000, 9000, "gyroX_f", "gyro axis X, filtered -20000 to 20000 0.1 dps"},
        {-9000, 9000, "gyroY_f", "gyro axis Y, filtered -20000 to 20000 0.1 dps"},
        {-9000, 9000, "gyroZ_f", "gyro axis Z, filtered -20000 to 20000 0.1 dps"},
        {0, 0, "Reserved", "Reserved"},
        {0, 0, "Reserved", "Reserved"},
        {0, 0, "Reserved", "Reserved"},
        // or{-32768, 32767, "motorEMFright_f", "right motor back EMF, filtered -32768 to 32767 22.5 cm"},
        // or{-32768, 32767, "motorEMFleft_f", "left motor back EMF, filtered -32768 to 32767 22.5 cm"},
        {-120, 120, "motorEMFright_f", "right motor back EMF, filtered -32768 to 32767 22.5 cm"},
        {-120, 120, "motorEMFleft_f", "left motor back EMF, filtered -32768 to 32767 22.5 cm"},
        {0, 0, "Reserved", "Reserved 1"},
        {0, 0, "Reserved", "Reserved 2"},
        {0, 0, "Reserved", "Reserved 3"},
        {0, 0, "Reserved", "Reserved 4"},
        {0, 0, "Reserved", "Reserved 5"}
    };

    const std::vector<sensorT> mask2meta = {
        {-10000, 10000, "q0", "Quaternion Q0 -10000 to 10000 1/10000 Q"},
        {-10000, 10000, "q1", "Quaternion Q1 -10000 to 10000 1/10000 Q"},
        {-10000, 10000, "q2", "Quaternion Q2 -10000 to 10000 1/10000 Q"},
        {-10000, 10000, "q3", "Quaternion Q3 -10000 to 10000 1/10000 Q"},
        {-32768, 32767, "odomX", "Odometer X -32768 to 32767 cm"},
        {-32768, 32767, "odomY", "Odometer Y -32768 to 32767 cm"},
        // or{0, 8000, "accelOne", "AccelOne 0 to 8000 1 mG"},
        {0, 1400, "accelOne", "AccelOne 0 to 8000 1 mG"},
        // or{-32768, 32767, "veloX", "Velocity X -32768 to 32767 mm/s"},
        // or{-32768, 32767, "veloY", "Velocity Y -32768 to 32767 mm/s"}
        {-1500, 1500, "veloX", "Velocity X -32768 to 32767 mm/s"},
        {-1500, 1500, "veloY", "Velocity Y -32768 to 32767 mm/s"}
    };
};

} // namespace sphero

#endif // SPHERO_H
