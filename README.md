The Sphero++ library serves as an interface between a Sphero robot and a Linux machine. It is currently used for investigating intrinsically motivated autonomous robots (more about that on [mms.ai](https://mms.ai) soon). I also used it for starting with library development in C++, so the current state (older than 2 years) may occur a bit messy.

It is tested and developed for the BB8 version of Sphero, which uses Bluetooth Low Energy (BLE) for communication.

# Coverage

The robot gets set into development mode after conecting, allowing for using the official [API](http://sdk.sphero.com/api-reference/api-quick-reference/).

So far, only stream data is parsed set up with `setDataStreaming`. Also implemented:

- `ping`
- `roll`
- `setRawMotorValues`
- `setRGBLEDoutput`

- `setStabilization`
- ~~`setRotationRate`~~
- `setInactivityTimeout`

# Dependencies

- [libblepp](https://github.com/edrosten/libblepp) (building / running)
- CMake (building)

# Examples

There are currently two examples `roll` and `stream-sensordata` for getting started. When executing they'll need a valid Mac address, e.g,, do `./roll AA:BB:CC:DD:EE:FF`.


