#include <sphero/sphero.h>

#include <iostream>
#include <unistd.h> /* sleep */

int main(int argc, char *argv[])
{

    if ( argc != 2 ) {
        std::cout << "Please provide an address to a Sphero." << std::endl;
        return 1;
    }

    std::vector<std::vector<int16_t>> currentStreamData;

    sphero::Sphero sp;

    // callback when data received
    std::function<void(std::vector<int16_t> &data)> onReceive = [&currentStreamData](std::vector<int16_t> &data) {

        currentStreamData.push_back(data);

    };

    // mac address
    std::string mac = argv[1];

    // callback when connection established
    std::function<void()> onConnect = []() {
        std::cout << "Sphero connected" << std::endl;
    };

    // connect to a Sphero
    sp.connectBlocking(mac, onReceive, onConnect);

    std::vector<uint8_t> mask = {0x00, 0x78, 0x00, 0x00};
    std::vector<uint8_t> mask2 ={0x03, 0x80, 0x00, 0x00};

    sp.setDataStreaming(mask, mask2);

    // keep alive
    for(;;) {
        usleep(40);
        sp.read();

        while (!currentStreamData.empty()) {

          std::vector<int16_t> data = currentStreamData.back();

          for(int i = 0; i < data.size(); i++) {
            std::cout << sp.getSensorShortName(i) << ": " << data.at(i) << " | ";
          }

          std::cout << std::endl;

          currentStreamData.pop_back();

        }

    }

}
