#include <sphero/sphero.h>

// HACK
#include <unistd.h> /* sleep */

using namespace std;
using namespace BLEPP;
using namespace sphero;
using namespace sphero::utils;

Sphero::Sphero() :
    _deviceFound(false),
    _streamingMaskVector(8, 0),
    _streamingMask(0),
    _streamingMask2(0),
    _streamDataMeta(0)
{
    // log_level = Info;

    _gatt.cb_disconnected = [](BLEGATTStateMachine::Disconnect d)
    {
        cerr << BLEGATTStateMachine::get_disconnect_string(d) << endl;
    };

}

Sphero::~Sphero() {

    _deviceFound = false;

}

void Sphero::connectBlocking(const std::string mac_address, std::function<void(std::vector<int16_t> &data)> onReceive, std::function<void()> onConnect) {

    _onReceive = onReceive;

    // TODO some check if already connected

    std::function<void(const PDUNotificationOrIndication&)> notify_cb = [&](const PDUNotificationOrIndication& n)
    {

    // debug
    // for (int i = 0; i < num - 1; i++) {
    // printf("%.2X", d[i]);
    // }

        parse(n.value().first, n.num_elements() - 1);

    };

    std::function<void()> found_services_and_characteristics_cb = [this, &notify_cb](){

        // pretty_print_tree(_gattBLE);

        for(auto& service: _gatt.primary_services)

            for(auto& characteristic: service.characteristics)

                if(service.uuid == sCtrl && characteristic.uuid == cRsp)
                {
                    characteristic.cb_notify_or_indicate = notify_cb;
                    characteristic.set_notify_and_indicate(true, false);
                }

        _deviceFound = true;

    };

    _gatt.setup_standard_scan(found_services_and_characteristics_cb);

    // connect blocking, set remote address random
    _gatt.connect(mac_address, true, false);

    while (_deviceFound == false) {
           _gatt.read_and_process_next();
    }

    putInDevMode();

    // HACK should wait for response of dev mode
    sleep(3);

    onConnect();
}

void Sphero::putInDevMode() {

    // ADoS (handle 42) set to 011i3
    const uint8_t bufA[5] = { (uint8_t)(0x30), (uint8_t)(0x31), (uint8_t)(0x31), (uint8_t)(0x69), (uint8_t)(0x33)};
    _gatt.send_write_command(42, bufA,5);
    _gatt.wait_on_write();

    // TXP: 23
    const uint8_t bufT[1] = { (uint8_t)(0x07)};
    _gatt.send_write_command(23, bufT, 1);
    _gatt.wait_on_write();

    // Wake: 47
    const uint8_t bufW[1] = { (uint8_t)(0x01)};
    _gatt.send_write_command(47, bufW, 1);
    _gatt.wait_on_write();
}

void Sphero::read() {

    fd_set write_set, read_set;

    // init file descriptors
    FD_ZERO(&read_set);
    FD_ZERO(&write_set);

    // set read bit
    FD_SET(_gatt.socket(), &read_set);

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 10000;
    int result = select(_gatt.socket() + 1, &read_set, &write_set, NULL, & tv);

    if(FD_ISSET(_gatt.socket(), &read_set))
        _gatt.read_and_process_next();
}

// Protocol

void Sphero::parse(const uint8_t* d, const int length) {

    /*
     * This checks for validity of received packages,
     * get the data and stores them locally and/or
     * emits a signal
     * Bluetooth packages can contain several streaming packages
     *
     * Asynchronous message packet looks like:
     * [ SOP1 | SOP2 | ID_CODE | DLEN (2B) | DATA (16bit signed each) | CHK ]
     *
     * state: m_buffer empty | (not yet) valid package
     * value: contains rest and/or valid package
     *
     */

    std::copy(d, d + length, std::back_inserter(_buffer));

    // for (int i = 0; i < _buffer.size(); i++) {
    //   printf("%.2X", _buffer.at(i));
    // }

    int type = isValidResponse(_buffer);

    if (type) {

        std::vector<uint8_t> validPkg = extractPackageFromBuffer(_buffer, type);

        if (validPkg.at(validPkg.size()-1) == checksum(validPkg, 1)) {
            // Compare cheksum byte with calculated checksum of
            // pkg without SOP bytes (-2) and CHK byte (-1)

          // if (type == 1) parseACK(validPkg);  nothing for now
          if (type == 2) parseASYNC(validPkg);
        }

        // TODO
        // if checksum of supposingly valid package is wrong, do:
        // 1) ignore it (like now)
        // 2) maybe check if there is a valid header insider and append to buffer again
        return;
    }

    if (isInvalidResponse(_buffer)) {
        // ignore clearly invalid packets
        _buffer.clear();
        return;
    }

    // it type == 0 and pkLen < pkg.szie erase too
    // Hack
    // for packages like FFFF0000 e.g. with wrong CID or DID ... can happen on start and in between
    // logic should go somewhere else
    if (!type && _buffer.size() > 4) {

        // cannot use responseType(_buffer);, might be 0
        int pkgLen = 0;
        int pkgLen1 = getExpectedSize(_buffer, 1);
        int pkgLen2 = getExpectedSize(_buffer, 2);

        if (pkgLen1 < pkgLen2) {
            pkgLen = pkgLen2;
        } else {
            pkgLen = pkgLen1;
        }

        if (pkgLen < _buffer.size())
            _buffer.erase(_buffer.begin(), _buffer.end());
    }

}

bool Sphero::isInvalidResponse(const std::vector<uint8_t> &pkg) const {

    assert(!pkg.empty());

    if (pkg.size() == 1) {

        if (pkg.front() != sop1) {
            return true;
        }
    }

    if (pkg.size() >= 2) {
    // Packet is at least 2 bytes long

        if (responseType(pkg) == 0) {
        // Packet has no valid header
                return true;
        }
    }

    return false;
}

void Sphero::parseASYNC(const std::vector<uint8_t> &value) {

    const int idCode = utils::scharToUint8(value, 2);

    // Streaming Data
    // each value is 16-bit signed integer
    if (idCode == 0x03) {

        vector<int16_t> data = getAsyncPayload(value);

        // get name

        // signal / call lambda
        _onReceive(data);

    }
}


vector<int16_t> Sphero::getAsyncPayload(const vector<uint8_t> &ba) {

    /*
     * Extracts data package of from byte array
     * [ SOP1 | SOP2 | ID_CODE | DLEN (2B) | DATA (16bit signed each) | CHK ]
     */

    vector<int16_t> data;

    const int dLen = scharToUint16(ba, 3);

    // mask
    for (int i = 5; i < dLen-1+5; i=i+2) {

        data.push_back(utils::scharToInt16(ba, i));

    }

    return data;
}



std::vector<u_int8_t> Sphero::extractPackageFromBuffer(std::vector<uint8_t> &pkg, const int type) {

    int size = getExpectedSize(pkg, type);

    vector<uint8_t> temp(pkg.begin(), pkg.begin() + size);

    pkg.erase(pkg.begin(), pkg.begin() + size);

    return temp;

}

int Sphero::isValidResponse(const std::vector<uint8_t> &pkg) const {

    if (pkg.size() > 5) {
        // Packet is at least 6 bytes long

        int type = responseType(pkg);

        if (type) {
            // Packet has valid async
            // message or acknowledgment header

            int pkgLen = getExpectedSize(pkg, type);

            if (pkg.size() >= pkgLen) {
                // If the buffer is at least of length
                // specified in the DLEN value the buffer
                // is valid (deal with extra bytes later)
                return type;
            }
        }
    }

    return false;

}

int Sphero::getExpectedSize(const std::vector<uint8_t> &pkg, const int type) const {

    if (type == 1) {

        int dlen = utils::scharToUint8(pkg, 4);

        return dlen + 5; // SOP1, SOP2, MRSP, SEQ, DLEN
    }

    if (type == 2) {

        int dlen = utils::scharToUint16(pkg, 3);

        return dlen + 5; // Sop 2B, idcode, dlen 2B
    }

    assert(type == 1 || type == 2);
    exit(-1);

}

int Sphero::responseType(const std::vector<uint8_t> &pkg) const {

    /*
     * 0: not valid
     * 1: acknowledgment
     * 2: async message
     *
     */

    if (pkg.size() < 2) {
        return 0;
    }

    const uint8_t _sop1 = pkg.at(0);
    const uint8_t _sop2 = pkg.at(1);

    if (_sop1 == sop1) {

        if (_sop2 == sop2ackno) return 1;
        if (_sop2 == sop2async) return 2;
    }

    return 0;

}

void Sphero::sendCommand(const uint8_t did, const uint8_t cid, std::vector<uint8_t> &data, bool requestACK) {

    /*
     * client command packet
     *                            1                         2       3
     * Steps:    +--------------------------------------+--------+-----+
     * Protocol: | SOP1 | SOP2 | DID | CID | SEQ | DLEN | <data> | CHK |
     *
     */

    // sets flag for requesting an ack package
    uint8_t sop2 = sop2async | requestACK;

    // 1 create header
    const std::vector<uint8_t> header = {
        sop1, sop2,
        did, cid, 0,
        (uint8_t)(data.size() + 1)
    };

    // 2 concat with data
    data.insert(data.begin(), header.begin(), header.end());

    // 3 calculate checksum
    data.insert(data.end(), checksum(data));

    const uint8_t* buf = &data[0];
    _gatt.send_write_command(14, buf, data.size());

}

void Sphero::sendCommandBlocking(const uint8_t did, const uint8_t cid, std::vector<uint8_t> &data, bool requestACK) {

    sendCommand(did, cid, data, requestACK);
    _gatt.wait_on_write();

}

// API

void Sphero::ping() {

    ping(true);
}

void Sphero::keepalive() {

    ping(false);
}

void Sphero::ping(const bool reqAck) {

    std::vector<uint8_t> data = {};

    sendCommand(0x00, 0x01, data, reqAck);
}

void Sphero::roll(const uint8_t speed, const uint16_t heading, const bool reqACK) {

    std::vector<uint8_t> data;

    data.push_back(speed);
    data.push_back(heading >> 8);
    data.push_back(heading >> 0);
    data.push_back(0x01); // Sate, what?

    sendCommand(0x02, 0x30, data, reqACK);
}

void Sphero::setStabilization(const bool flag) {

    std::vector<uint8_t> data;

    data.push_back(flag);

    sendCommand(0x02, 0x02, data);

}

// http://sdk.sphero.com/api-reference/api-quick-reference/#sphero-set-rgb-led-output-20h
void Sphero::setRGBLEDoutput(const uint8_t r, const uint8_t g, const uint8_t b, const bool persistent) {

  std::vector<uint8_t> data;

  data.push_back(r);
  data.push_back(g);
  data.push_back(b);
  data.push_back(persistent);

  sendCommandBlocking(0x02, 0x20, data);

}

// http://sdk.sphero.com/api-reference/api-quick-reference/#sphero-set-rotation-rate-03h
// value degree/second
void Sphero::setRotationRate(const uint degreesPerSecond) {

  /*
   * The commanded value is in units of 0.784 degrees/sec.
   * So, setting a value of C8h will set the rotation rate to 157 degrees/sec.
   * A value of 255 jumps to the maximum (currently 400 degrees/sec). A value of
   * zero doesn't make much sense so it's interpreted as 1, the minimum.
  */
    uint8_t value = (double)degreesPerSecond / 0.784;

    std::vector<uint8_t> data;

    data.push_back(value);

    std::cout << "data rotation: " << (int)data[0] << std::endl;

    sendCommandBlocking(0x02, 0x03, data);

}

void Sphero::setInactivityTimeout(const uint16_t seconds) {

  /*
   * http://sdk.sphero.com/api-reference/api-quick-reference/#core-set-inactivity-timeout-25h
   *
   * To save battery power, Sphero normally goes to sleep after a period of inactivity.
   * From the factory this value is set to 600 seconds (10 minutes)
   * but this API command can alter it to any value of 60 seconds or greater.
   */

  std::vector<uint8_t> data;

  data.push_back(splitUint16(seconds).first);
  data.push_back(splitUint16(seconds).second);

  std::cout << "data inactivity: " << (int)data[0] << (int)data[1] << std::endl;


  sendCommandBlocking(0x00, 0x25, data);

}

void Sphero::setDataStreaming(const uint32_t mask, const uint32_t mask2, const uint freq) {

    _streamingMask  = mask;
    _streamingMask2 = mask2;

    std::vector<uint8_t> data;

    // 400Hz / (40 (28h) | 400 (190h) )
    uint16_t n = 400/freq;

    // N
    data.push_back(splitUint16(n).first);
    data.push_back(splitUint16(n).second);

    // M
    data.push_back(0x00);
    data.push_back(0x01);

    // Mask
    data.push_back((uint8_t)(mask >> 24));
    data.push_back((uint8_t)(mask >> 16));
    data.push_back((uint8_t)(mask >> 8));
    data.push_back((uint8_t)(mask));

    // PCNT
    data.push_back(0x00); // unlimited

    // Mask2 (optional)
    if (mask2 != 0) {
        data.push_back((uint8_t)(mask2 >> 24));
        data.push_back((uint8_t)(mask2 >> 16));
        data.push_back((uint8_t)(mask2 >> 8));
        data.push_back((uint8_t)(mask2));
    }

    //
    // compute _streamData
    //
    uint mask1bits = utils::numberOfSetBits(_streamingMask);
    uint mask2bits = utils::numberOfSetBits(_streamingMask2);

    int index = 31;

    while(mask1bits > 0) {

        while ( index >= 0 && mask1bits > 0) {

            if ((uint32_t)(1 << index)  ==  (_streamingMask & (uint32_t)(1 << index) & (uint32_t)(1 << index))) {
                mask1bits--;
                _streamDataMeta.push_back(getSensorMeta(31-index));
            }
            index--;
        }
    }


    index = 31;

    while(mask2bits > 0) {

        while ( index >= 0 && mask2bits > 0) {

            if ((uint32_t)(1 << index)  ==  (_streamingMask2 & (uint32_t)(1 << index) & (uint32_t)(1 << index))) {
                mask2bits--;
                _streamDataMeta.push_back(getSensorMeta(31-index, true));
            }
            index--;
        }
    }

    sendCommandBlocking(0x02, 0x11, data);

}


void Sphero::setDataStreaming(const std::vector<uint8_t> &mask, const std::vector<uint8_t> &mask2, const uint freq) {

    assert(mask.size() == 4);
    assert(mask2.size() == 4 || mask2.size() == 0);

    /*
     * | DLEN       |   N          |	M       | MASK       | PCNT      | MASK2       |
     * | 0ah or 0eh |	16-bit val | 16-bit val | 32-bit val | 8-bit val |	32-bit val |
     *
     * More: https://sdk.sphero.com/api-reference/api-quick-reference/#sphero-set-data-streaming-11h
     */


    // compute a concatenated vector
//    _streamingMaskVector.insert(_streamingMaskVector.begin(), mask.begin(), mask.end());

//    if (mask2.size() == 4) {
//        _streamingMaskVector.insert(_streamingMaskVector.end(), mask2.begin(), mask2.end());
//    } else {
//        for (uint i = 0; i < 4; i++)
//            _streamingMaskVector.push_back(0x00);
//    }

    // compute a bitmask
    // TODO just send it as a bitmask
    uint32_t maskmask  = mask.at(0) << 24 | mask.at(1) << 16 | mask.at(2) << 8 | mask.at(3);
    uint32_t maskmask2 = mask2.at(0) << 24 | mask2.at(1) << 16 | mask2.at(2) << 8 | mask2.at(3);

    setDataStreaming(maskmask, maskmask2, freq);

}

void Sphero::setRawMotorValues(const uint8_t lMode, const uint8_t lPower, const uint8_t rMode, const uint8_t rPower, const bool reqACK) {
    /*
     *  https://sdk.sphero.com/api-reference/api-quick-reference/#sphero-set-raw-motor-values-33h
     *
     *  DID	CID	SEQ	DLEN	L-MODE	L-POWER	R-MODE	R-POWER
     *  02h	33h		05h	       val	    val	   val	    val
     */


    // --- check for change

    static uint8_t lMode_last  = 0x04;
    static uint8_t lPower_last = 0x00;
    static uint8_t rMode_last  = 0x04;
    static uint8_t rPower_last = 0x00;


//    std::cout << (int)lMode_last  << " lM " << (int)lMode << std::endl;
//    std::cout << (int)lPower_last << " lP " << (int)lPower << std::endl;
//    std::cout << (int)rMode_last  << " rM " << (int)rMode << std::endl;
//    std::cout << (int)rPower_last << " rP " << (int)rPower << std::endl;


    uint8_t tmp_lMode = lMode;
    uint8_t tmp_rMode = rMode;

    // if nothing changed
    if (lMode_last == lMode && lPower_last == lPower)
        tmp_lMode = 0x04; // motor mode and power is left unchanged

    if  (rMode_last == rMode && rPower_last == rPower)
        tmp_rMode = 0x04; // motor mode and power is left unchanged

    // --------------

    // avoiding sending messages with two unchaged modes -> may trigger stabilization
    if (tmp_lMode != 0x04 || tmp_rMode != 0x04) {

        std::vector<uint8_t> data;
        data.push_back(tmp_lMode);
        data.push_back(lPower);
        data.push_back(tmp_rMode);
        data.push_back(rPower);

        sendCommand(0x02, 0x33, data, reqACK);

        // std::cout << "SENT: lMode: " << (int)tmp_lMode << " | lPower: " << (int)lPower << " | rMode" << (int)tmp_rMode << " | rPower: " << (int)rPower << std::endl;

    }

    lMode_last  = lMode;
    lPower_last = lPower;
    rMode_last  = rMode;
    rPower_last = rPower;

}
