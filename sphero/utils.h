#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include <vector>
#include <string>

namespace sphero {
namespace utils {

    uint8_t checksum(const std::vector<uint8_t> &data, const bool containsCHK = false);

    uint8_t scharToUint8(const std::vector<uint8_t> &c, const int start);
    uint16_t scharToUint16(const std::vector<uint8_t> &c, const int start);
    int16_t  scharToInt16(const std::vector<uint8_t> &c, const int start);

    int numberOfSetBits(uint32_t i);

    std::pair<uint8_t, uint8_t> splitUint16(const uint16_t &);
}
}

#endif // UTILS_H
